This scripts primary use is for keeping Windows awake when you are unable to alter the power settings in Windows.
It essentially simulates a mouse click at a specific point on the screen, which is declared with the $X , $Y variables, it also controls the loop using sleep -Seconds X, basically pausing the script for X amount of seconds and then beginning the loop again.

The script is written primarily in .NET, but is essentially wrapped in PowerShell (.ps1) format.

If you get any errors in relation to scripts not being allowed to run on a system, open an elevated (admin) PowerShell prompt and type the following command Set-ExecutionPolicy -Unrestricted, then try run the script again.

Any issues let me know at scripts@liamodonnell.uk

Liam.
